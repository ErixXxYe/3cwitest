package Camera;

public class Pictures {
    private int size;
    private String date, name;
    private  Setting setting;

    public Pictures(Setting setting, String date, String name) {
        this.date = date;
        this.name = name;
        this.setting = setting;
    }

    public String getName(){
        return name;
    }

    public String getInfo(){
        return setting.getSetting() + " " + this.name + " " + this.date;
    }

    public int getSize(){
        return setting.getSize();
    }

}
