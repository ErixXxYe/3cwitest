package Camera;

import java.util.ArrayList;
import java.util.List;
import static Camera.Camera.SpaceEntire;
import static Camera.Camera.picNum;

public class SDCard {
    private int capacity;
    private List<Pictures>files;
    private Pictures pictures;

    public SDCard(int capacity) {
        this.capacity = capacity;
        this.files = new ArrayList<>();
    }

    public void getAllPic(){
        for (Pictures picture: files){
            System.out.println(picture.getInfo());
        }
    }

    public void saveFile(Pictures picture){
        files.add(picture);
    }

    public int getCapacity(){
        int space = 0;
        for(Pictures picture: files){
            space += picture.getSize();
        }
        System.out.println("your capacity is " + (capacity - space));
        return capacity - space;
    }
    public int checkCapacity(){
        int space = 0;
        for(Pictures picture: files){
            space += picture.getSize();
        }
        if (capacity - space < 0){
            files.remove(picNum-1);
            picNum -= 1;
            System.out.println("picture too big, please check you Capacity");
            return SpaceEntire = 1;
        } else if (capacity - space == 0){
            return SpaceEntire = 1;
        } else {
            return SpaceEntire = 0;
        }
    }
}


