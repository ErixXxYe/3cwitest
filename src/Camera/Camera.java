package Camera;

import org.w3c.dom.ls.LSOutput;

public class Camera {
    private int pixel, weight;
    private String color;
    private Producer producer;
    private Lens lens;
    private SDCard sdCard;
    static int picNum = 0;
    static int SpaceEntire = 0;


    public Camera(int pixel, int weight, String color, Producer producer, Lens lens, SDCard sdCard) {
        this.pixel = pixel;
        this.weight = weight;
        this.color = color;
        this.producer = producer;
        this.lens = lens;
        this.sdCard = sdCard;
    }

    public Pictures takePicture(Setting setting) {
        sdCard.checkCapacity();
        if (SpaceEntire == 0) {
            Pictures pictures = new Pictures(setting, "31st Feb", "pic" + picNum);
            picNum++;
            sdCard.saveFile(pictures);
            sdCard.checkCapacity();
            if (SpaceEntire == 0) {
                System.out.println(pictures.getInfo());
            }
            return pictures;
        } else {
            System.out.println("I can't take Pics the Capacity is full ");
            System.out.println(sdCard.getCapacity() );
            Pictures pictures = new Pictures(setting, " ", " ");
            picNum--;
            return pictures;

        }
    }

    public void getAllPic(){
        sdCard.getAllPic();
    }

    public void getCapacity(){
        sdCard.getCapacity();
    }

    public void setSdCard(SDCard sdCard) {
        this.sdCard = sdCard;
        picNum = 0 ;
    }
}
