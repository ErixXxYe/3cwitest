package Camera;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Producer p1 = new Producer("Hartlauer", "Detuschland");
        SDCard sdc1 = new SDCard(16000);
        SDCard sdc2 = new SDCard(16000);
        Lens l1 = new Lens(10, p1);
        Setting sLow = new Setting("Low", 2000, 440);
        Setting sMedium = new Setting("Medium", 4000, 720);
        Setting sHigh = new Setting("High", 6000, 1080);
        Camera c1 = new Camera(1080, 2000, "red", p1, l1, sdc1);
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.println(" ");
            System.out.println("press 1 to take a picture");
            System.out.println("press 2 to check your Capacity");
            System.out.println("press 3 to take a picture");
            System.out.println("press 4 to change SDCard");
            switch (input.nextInt()){
                case 1:
                    System.out.println("press 1 for low Quality");
                    System.out.println("press 2 for medium Quality");
                    System.out.println("press 3 for high Quality");
                    switch (input.nextInt()){
                        case 1:
                            c1.takePicture(sLow);
                            break;
                        case 2:
                            c1.takePicture(sMedium);
                            break;
                        case 3:
                            c1.takePicture(sHigh);
                            break;
                    }
                    break;
                case 2:
                    c1.getCapacity();
                    break;
                case 3:
                    c1.getAllPic();
                    break;
                case 4:
                    System.out.println("press 1 to change SDCard to SDC1");
                    System.out.println("press 2 to change SDCard to SDC2");
                    switch (input.nextInt()){
                        case 1:
                            c1.setSdCard(sdc1);
                            break;
                        case 2:
                            c1.setSdCard(sdc2);
                            break;
                    }
            }

        }
    }
}
