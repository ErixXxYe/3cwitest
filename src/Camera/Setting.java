package Camera;

public class Setting {
    private String setting;
    private int size;
    private int resolution;

    public Setting(String setting, int size, int resolution) {
        this.setting = setting;
        this.size = size;
        this.resolution = resolution;
        }

        public String getSetting(){
        return this.setting + " " + this.size + " " + this.resolution;
        }

        public int getSize(){
            return size;
        }

    }

