package Phone;

class Files {
    private int size;
    private String name;
    private String extension;

    public Files(int size, String name, String extension){
        this.size = size;
        this.name = name;
        this.extension = extension;
    }

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public String getExtension() {
        return extension;
    }


    public String getAllFiles(){
        return this.name + " " + this.extension + " " + this.size;
    }

}
