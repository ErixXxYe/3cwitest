package Phone;

public class Camera {
    private int resolution;
    private int number;

    public Camera(int resolution) {
        this.resolution = resolution;
    }
    public Files takePicture() {
        Files file = new Files(100, "pic" + number, "jpg");
        number++;
        return file;
    }
}