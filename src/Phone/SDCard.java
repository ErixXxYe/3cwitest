package Phone;

import java.util.ArrayList;
import java.util.List;

public class SDCard {
    private int capacity;
    private List<Files>files;
    public SDCard ( int capacity){
        this.capacity = capacity;
        this.files = new ArrayList<>();
    }

    public void saveFile(Files file){
        files.add(file);
    }

    public int getFreeSpace(){
        int space = 0;
        for(Files file : files){
            space += file.getSize();
        }
        return capacity - space;
    }

    public List<Files> getAllFiles(){
        return files;
    }

}
