package Phone;

import java.util.List;

public class Phone {
    private Sim sim;
    private Camera camera;
    private SDCard sdCard;

    public Phone  (Sim sim, Camera camera, SDCard sdcard){
        this.sim = sim;
        this.camera = camera;
        this.sdCard = sdcard;
    }

    public void takePic(){
        Files file = camera.takePicture();
        sdCard.saveFile(file);
        System.out.println(file.getName());
    }

    public void makeCall(String number){
        sim.doCall(number);
    }

    public void checkSpace(){
        System.out.println(sdCard.getFreeSpace());
    }

    public void getAllFiles(){
        List<Files> files = this.sdCard.getAllFiles();
        for(Files file : files){
            System.out.println(file.getAllFiles());
        }
    }

}
