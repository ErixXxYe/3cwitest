package Jukebox;

public class Songs {
    private int id;
    private String name;
    private int length;

    public Songs(int id, String name, int length) {
        this.id = id;
        this.name = name;
        this.length = length;
    }

    public int getSongLength(){
            return length;
    }

    public int getId() {
        return id;
    }

    public String getSong() {
        return this.id + " " + this.name + " "+ this.length;
    }
}
