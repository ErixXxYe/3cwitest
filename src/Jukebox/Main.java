package Jukebox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        MusicBox musicBox = new MusicBox();
        Songs s1 = new Songs(1,"bell",2);
        Songs s2 = new Songs(2,"Cat",4);
        Songs s3 = new Songs(1,"Hagstrom",3);
        Songs s4 = new Songs(2,"Tilt" , 2);
        List<Songs> songs = Arrays.asList(s1, s2 );
        List<Songs> songs2 = Arrays.asList(s3, s4 );
        MusicDisk musicDisk1= new MusicDisk(2,"NoHits", songs2);
        MusicDisk musicDisk2= new MusicDisk(3,"JustHits", songs);
        MusicDisk musicDisk = new MusicDisk(1,"Hits", songs );
        musicBox.addDisk(musicDisk);
        musicBox.addDisk(musicDisk2);
        musicBox.removeDisk(musicDisk);
        musicBox.getSumOfMusic();
        musicBox.addDisk(musicDisk1);
        musicBox.searchDisks("TopHits");
        musicBox.play(1);
    }
}
