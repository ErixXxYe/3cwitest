package Jukebox;

import java.util.ArrayList;
import java.util.List;

public class MusicBox {
    private List<MusicDisk> disklist;
    private MusicDisk musicDisk;
    //private int id = 1;
    private boolean space = true;
    private boolean diskLoaded = false;
    private MusicDisk loadedDisk;

    public MusicBox() {
        this.disklist = new ArrayList<>();
    }
    public void addDisk(MusicDisk musicDisk) {
        disklist.add(musicDisk);
        //musicDisk.setId(id);
        //id++;
        checkDiskAmount();

        if(space) {
            System.out.println("I have added " + musicDisk.getMusicDisk());
        }
    }

    public void removeDisk(MusicDisk musicDisk) {
        disklist.remove(musicDisk);
        System.out.println("I have removed " + musicDisk.getMusicDisk());
    }

    public void searchDisks(String diskName){
        for ( MusicDisk musicDisk : disklist){
            if (musicDisk.getMusicDiskName() == diskName){
                System.out.println(musicDisk.getMusicDisk() );
            }
        }
    }

    public void getAllDisks() {
        for (MusicDisk musicDisk : disklist) {
            System.out.println(musicDisk.getMusicDisk());
        }
    }

    public void getSumOfMusic(){
        int sumOfMusic = 0;
        for (MusicDisk musicDisk : disklist){
            sumOfMusic += musicDisk.getSumMusicLength();
        }
        System.out.println("The length of all songs consits of "+sumOfMusic + " Minutes");
    }

    public void checkDiskAmount(){
        int disks = 0;
        for (MusicDisk musicDisk : disklist) {
            disks++;
        }
        if (disks > 50){
            disklist.remove(50);
            System.out.println("Nicht genug platz bb");
            space = false;
        }
    }

    public void loadDisk(String nameOfDisk) {
        for (MusicDisk musicDisk : disklist) {
            if (musicDisk.getMusicDiskName() == nameOfDisk) {
                System.out.println("Die Disk " + musicDisk.getMusicDisk() + " ist geladen");
                loadedDisk = musicDisk;
                diskLoaded = true;
            }
            if (loadedDisk == null){
                diskLoaded = false;
            }
        }
    }

    public void play(int num){
        if(diskLoaded) {
            loadedDisk.play(num);
        } else{
            System.out.println("No disk loaded");
        }

    }

    public boolean isDiskLoaded() {
        return diskLoaded;
    }

    public void setDiskLoaded(boolean diskLoaded) {
        this.diskLoaded = diskLoaded;
    }
}
