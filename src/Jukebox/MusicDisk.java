package Jukebox;

import java.util.ArrayList;
import java.util.List;
public class MusicDisk {
    private MusicBox musicBox;
    private int id;
    private String name;
    private List<Songs> songsList;

    public MusicDisk( int id,String name, List<Songs> songs) {
        this.id = id;
        this.name = name;
        this.songsList = songs;
    }

    public String getMusicDiskName(){
        return this.name;
    }

    public void play(int id){
        for( Songs songs : songsList){
            if (songs.getId() == id){
                System.out.println("playing song " + songs.getSong());
            }
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getSumMusicLength(){
        int length = 0;
        for ( Songs songs : songsList){
            length += songs.getSongLength();
        }
        return length;
    }

    public String getMusicDisk(){
        return this.id + " " + this.name;
    }

}
